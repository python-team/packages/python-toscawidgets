Source: python-toscawidgets
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper (>= 7),
 cdbs,
 python-dev,
 python-setuptools
Standards-Version: 3.8.2
Homepage: http://toscawidgets.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-toscawidgets.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-toscawidgets

Package: python-toscawidgets
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
 python-webob,
 python-simplejson (>= 2.0),
 python-formencode (>= 1.1)
Suggests:
 python-cheetah,
 python-genshi,
 python-kid
Description: Python framework for building reusable web components
 ToscaWidgets is a system for creating re-usable web components, such
 as a popup calendar or a rich text editor.
 .
 In general, components consist of an HTML template, static resources
 (JavaScript, CSS, Images, etc.) and server-side code. ToscaWidgets
 allow these to be packaged into a "widget", that a web developer can
 then easily use in their application.
 .
 This package also provides some contributions for ToscaWidgets, in
 particular:
 .
  * tw.forms - web widgets for building and validating forms
